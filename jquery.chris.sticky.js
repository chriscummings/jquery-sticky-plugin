/*
Sticky - jQuery Plugin

Use Examples:
================================================================================
$('#element').sticky({
	bottom: true,
	right: true
});

$('#element').sticky({
	top: false,
	bottom: true,
	right: false,
	left: false
});

Source
================================================================================
http://github.com/chriscummings/jquery-sticky-plugin

*/

'use strict';

(function ($) {	
	$.fn.extend({ 
		sticky: function (options) {
			var defaults,
				//options,
				getWindowWidth,
				getWindowHeight,
				getElementHeight,
				getElementWidth,
				position;
				
			defaults = {
				top: false,
				bottom: false,
				left: false,
				right: false,
				resize: true
			};
							
			options =  $.extend(defaults, options);
			
			getWindowWidth = function () {
				return $(window).width();
			};
			
			getWindowHeight = function () {
				return $(window).height();
			};
			
			getElementHeight = function (element) {
				return $(element).outerHeight();
			};
			
			getElementWidth = function (element) {
				return $(element).outerWidth();
			};

			position = function (element, options) {
				if (options.bottom) {
					$(element).css({
						'top': (getWindowHeight() - getElementHeight(element))
					});
				} else if (options.top) {
					$(element).css({
						'top': '0px'
					});
				}
				
				if (options.left) {
					$(element).css({
						'left': '0px'
					});
				} else if (options.right) {					
					$(element).css({
						'left': (getWindowWidth() - getElementWidth(element))
					});
				}
			};

			return this.each(function () {
				var element = $(this);
				
				$(element).css({'position': 'absolute'});				
				
				position(element, options);
	
				if (options.resize) {
					$(window).resize(function () {
						
						setTimeout(function () {
							position(element, options);
						}, 100);
						
					});					
				}
			});
		}
	});
})(jQuery);